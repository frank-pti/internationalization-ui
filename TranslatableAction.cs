/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable action.
    /// </summary>
    public class TranslatableAction: Gtk.Action
    {
        private TranslationString translatableLabel;
        private TranslationString translatableToolTip;

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableAction"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="arguments">Arguments.</param>
        public TranslatableAction(string name, I18n i18n, string resource, string text, params string[] arguments):
            this(name, i18n.TrObject(resource, text, arguments))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableAction"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="translatableLabel">Translatable label.</param>
        public TranslatableAction(string name, TranslationString translatableLabel):
            this(name, translatableLabel, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableAction"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="translatableLabel">Translatable label.</param>
        /// <param name="toolTip">Tool tip.</param>
        public TranslatableAction(string name, TranslationString translatableLabel, TranslationString toolTip):
            base(name, translatableLabel.Tr, (toolTip == null ? null : toolTip.Tr), null)
        {
            TranslatableLabel = translatableLabel;
            TranslatableToolTip = toolTip;
        }

        private void HandleTranslatableLabelChanged (TranslationString translationString)
        {
            Label = translatableLabel.Tr;
        }

        private void HandleTranslatableToolTipChanged (TranslationString translationString)
        {
            if (TranslatableToolTip == null) {
                Tooltip = null;
            } else {
                Tooltip = TranslatableToolTip.Tr;
            }
        }

        /// <summary>
        /// Gets or sets the translatable label.
        /// </summary>
        /// <value>The translatable label.</value>
        public TranslationString TranslatableLabel {
            get {
                return translatableLabel;
            }
            set {
                if (translatableLabel != null) {
                    translatableLabel.Changed -= HandleTranslatableLabelChanged;
                }
                translatableLabel = value;
                if (translatableLabel != null) {
                    translatableLabel.Changed += HandleTranslatableLabelChanged;
                    HandleTranslatableLabelChanged(translatableLabel);
                }
            }
        }

        /// <summary>
        /// Gets or sets the translatable tool tip.
        /// </summary>
        /// <value>The translatable tool tip.</value>
        public TranslationString TranslatableToolTip {
            get {
                return translatableToolTip;
            }
            set {
                if (translatableToolTip != null) {
                    translatableToolTip.Changed -= HandleTranslatableToolTipChanged;
                }
                translatableToolTip = value;
                if (translatableToolTip != null) {
                    translatableToolTip.Changed += HandleTranslatableToolTipChanged;
                    HandleTranslatableToolTipChanged(translatableToolTip);
                }
            }
        }
    }
}
