/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable expander.
    /// </summary>
    public class TranslatableExpander: Gtk.Expander
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableExpander"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="domain">Domain.</param>
        /// <param name="text">Text.</param>
        public TranslatableExpander (I18n i18n, string domain, string text):
            this(new TranslationString(i18n, domain, text))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableExpander"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        public TranslatableExpander (TranslationString translation):
            base("")
        {
            LabelWidget = new TranslatableLabel(translation);
        }

        /// <summary>
        /// Gets or sets the translation.
        /// </summary>
        /// <value>The translation.</value>
        public TranslationString Translation
        {
            get {
                return (LabelWidget as TranslatableLabel).Translation;
            }
            set {
                (LabelWidget as TranslatableLabel).Translation = value;
            }
        }
    }
}

