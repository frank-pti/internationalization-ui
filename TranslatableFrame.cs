/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Internationalization;
using Gtk;
using System;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable frame.
    /// </summary>
	public class TranslatableFrame: Frame
	{
		private TranslatableLabel label;
		private Alignment border;

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableFrame"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
		public TranslatableFrame(I18n i18n, string resource, string text):
			this(new TranslationString(i18n, resource, text))
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableFrame"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
		public TranslatableFrame(TranslationString translation): base()
		{
			label = new TranslatableLabel(translation);
			border = new Alignment(0f, 0f, 1f, 1f) {
				LeftPadding = 2,
				RightPadding = 2
			};
			border.Add(label);
			UpdateTitleVisibility();
			translation.Changed += HandleTranslationI18nChanged;
		}

		private void HandleTranslationI18nChanged (TranslationString translationString)
		{
			Application.Invoke(delegate {
				UpdateTitleVisibility();
			});
		}

        /// <summary>
        /// Gets or sets the translation.
        /// </summary>
        /// <value>The translation.</value>
		public TranslationString Translation
		{
			get {
				return label.Translation;
			}
			set {
				label.Translation = value;
			}
		}

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="InternationalizationUi.TranslatableFrame"/> use markup.
        /// </summary>
        /// <value><c>true</c> if use markup; otherwise, <c>false</c>.</value>
		public bool UseMarkup {
			get {
				return label.UseMarkup;
			}
			set {
				label.UseMarkup = value;
			}
		}

        /// <summary>
        /// Updates the title visibility.
        /// </summary>
		private void UpdateTitleVisibility()
		{
            LabelWidget = null;
			if (!String.IsNullOrWhiteSpace(label.Translation.Tr)) {
				LabelWidget = border;
				LabelWidget.ShowAll();
			}
		}
	}
}

