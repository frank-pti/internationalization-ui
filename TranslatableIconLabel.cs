using Gtk;
using Internationalization;
/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable icon label.
    /// </summary>
    public class TranslatableIconLabel : HBox
    {
        private const int DefaultSpacing = 3;

        private TranslatableLabel label;
        private Image icon;
        private Box box;

        /// <summary>
        /// Internal constructor to support wrapping of native object handles.
        /// </summary>
        /// <param name="raw">Pointer to the C object.</param>
        /// <remarks>This is an internal constructor, and should not be used by user code.
        /// It is required by the GTK# environment.</remarks>
        public TranslatableIconLabel(IntPtr raw) :
            base(raw)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconLabel"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="icon">Icon.</param>
        public TranslatableIconLabel(I18n i18n, string resource, string text, Image icon) :
            this(i18n, resource, text, icon, Orientation.Horizontal)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconLabel"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="orientation">Orientation.</param>
        public TranslatableIconLabel(I18n i18n, string resource, string text, Image icon, Orientation orientation) :
            this(new TranslationString(i18n, resource, text), icon, orientation)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconLabel"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        /// <param name="icon">Icon.</param>
        public TranslatableIconLabel(TranslationString translation, Image icon) :
            this(translation, icon, Orientation.Horizontal)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconLabel"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="orientation">Orientation.</param>
        public TranslatableIconLabel(TranslationString translation, Image icon, Orientation orientation) :
            this(translation, icon, orientation, new TranslatableLabel(translation))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconLabel"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="orientation">Orientation.</param>
        /// <param name="label">Label.</param>
        protected TranslatableIconLabel(TranslationString translation, Image icon, Orientation orientation, TranslatableLabel label)
        {
            switch (orientation) {
                case Orientation.Horizontal:
                    box = new HBox(false, DefaultSpacing);
                    break;
                case Orientation.Vertical:
                    box = new VBox(false, DefaultSpacing);
                    break;
                default:
                    throw new ArgumentException(String.Format("Invalid Gtk Orientation: {0}", orientation));
            }
            PackStart(box, true, false, 0);
            this.icon = icon;
            this.label = label;
            box.PackStart(icon);
            box.PackEnd(label);
            ShowAll();
        }

        /// <summary>
        /// Gets or sets the translation.
        /// </summary>
        /// <value>The translation.</value>
        public TranslationString Translation
        {
            get
            {
                return label.Translation;
            }
            set
            {
                label.Translation = value;
            }
        }

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>The icon.</value>
        public Image Icon
        {
            set
            {
                if (icon != null) {
                    box.Remove(icon);
                }
                icon = value;
                if (icon != null) {
                    box.PackStart(icon);
                }
            }
            get
            {
                return icon;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="InternationalizationUi.TranslatableIconLabel"/> use underline.
        /// </summary>
        /// <value><c>true</c> if use underline; otherwise, <c>false</c>.</value>
        public bool UseUnderline
        {
            set
            {
                label.UseUnderline = value;
            }
            get
            {
                return label.UseUnderline;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="InternationalizationUi.TranslatableIconLabel"/> is wrap.
        /// </summary>
        /// <value><c>true</c> if wrap; otherwise, <c>false</c>.</value>
        public bool Wrap
        {
            set
            {
                label.Wrap = value;
            }
            get
            {
                return label.Wrap;
            }
        }

        /// <summary>
        /// Gets or sets the line wrap mode.
        /// </summary>
        /// <value>The line wrap mode.</value>
        public Pango.WrapMode LineWrapMode
        {
            set
            {
                label.LineWrapMode = value;
            }
            get
            {
                return label.LineWrapMode;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="InternationalizationUi.TranslatableIconLabel"/> use markup.
        /// </summary>
        /// <value><c>true</c> if use markup; otherwise, <c>false</c>.</value>
        public bool UseMarkup
        {
            set
            {
                label.UseMarkup = value;
            }
            get
            {
                return label.UseMarkup;
            }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment of the text
        /// </summary>
        /// <value>The x align.</value>
        public float Xalign
        {
            get
            {
                return label.Xalign;
            }
            set
            {
                label.Xalign = value;
            }
        }

        /// <summary>
        /// Gets or sets the spacing.
        /// </summary>
        /// <value>The spacing.</value>
        public new int Spacing
        {
            get
            {
                return box.Spacing;
            }
            set
            {
                box.Spacing = value;
            }
        }
    }
}
