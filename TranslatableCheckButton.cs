/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;
using Internationalization;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable check button.
    /// </summary>
	public class TranslatableCheckButton: CheckButton
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableCheckButton"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="arguments">Arguments.</param>
		public TranslatableCheckButton(I18n i18n, string resource, string text, params object[] arguments):
			this(new TranslationString(i18n, resource, text, arguments))
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableCheckButton"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
		public TranslatableCheckButton(TranslationString translation): base()
		{
            Add (new TranslatableLabel(translation));
		}

        /// <summary>
        /// Gets or sets the child (a translatabel label)
        /// </summary>
        /// <value>The child.</value>
        public new Widget Child 
        {
            get {
                return base.Child;
            }
            set {
                if (value is TranslatableLabel) {
                    Child = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the label widget.
        /// </summary>
        /// <value>The label widget.</value>
        public TranslatableLabel LabelWidget
        {
            get {
                return (Child as TranslatableLabel);
            }
            set {
                Child = value;
            }
        }
	}
}

