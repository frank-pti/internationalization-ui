/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;
using Internationalization;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable icon toggle button.
    /// </summary>
	public class TranslatableIconToggleButton: ToggleButton
	{
        /// <summary>
        /// The label.
        /// </summary>
		protected TranslatableIconLabel label;

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconToggleButton"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="icon">Icon.</param>
		public TranslatableIconToggleButton(I18n i18n, string resource, string text, Image icon):
			this(new TranslationString(i18n, resource, text), icon, Orientation.Horizontal)
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconToggleButton"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        /// <param name="icon">Icon.</param>
		public TranslatableIconToggleButton(TranslationString translation, Image icon):
			this(translation, icon, Orientation.Horizontal)
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconToggleButton"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="orientation">Orientation.</param>
		public TranslatableIconToggleButton(I18n i18n, string resource, string text, Image icon, Orientation orientation):
			this(new TranslationString(i18n, resource, text), icon, orientation)
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconToggleButton"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="orientation">Orientation.</param>
		public TranslatableIconToggleButton(TranslationString translation, Image icon, Orientation orientation)
		{
			InitializeLabel(new TranslatableIconLabel(translation, icon, orientation));
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableIconToggleButton"/> class.
        /// </summary>
        /// <param name="label">Label.</param>
		protected TranslatableIconToggleButton(TranslatableIconLabel label)
		{
			InitializeLabel(label);
		}
		
        /// <summary>
        /// Initializes the label.
        /// </summary>
        /// <param name="label">Label.</param>
		protected virtual void InitializeLabel(TranslatableIconLabel label)
		{
			this.label = label;
			Add(label);
		}

        /// <summary>
        /// Gets or sets the translation.
        /// </summary>
        /// <value>The translation.</value>
		public TranslationString Translation
		{
			get {
				return label.Translation;
			}
			set {
				label.Translation = value;
			}
		}

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>The icon.</value>
		public Image Icon {
			set {
				label.Icon = value;
			}
			get {
				return label.Icon;
			}
		}
	}
}
