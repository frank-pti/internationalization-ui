/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;

namespace InternationalizationUi.Mnemonic
{
    /// <summary>
    /// Translatable mnemonic label.
    /// </summary>
	public class TranslatableMnemonicLabel: TranslatableLabel
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.Mnemonic.TranslatableMnemonicLabel"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
		public TranslatableMnemonicLabel(I18n i18n, string resource, string text):
			base(i18n, resource, text)
		{
			UseUnderline = true;
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.Mnemonic.TranslatableMnemonicLabel"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
		public TranslatableMnemonicLabel(TranslationString translation):
			base(translation)
		{
			UseUnderline = true;
		}

        /// <inheritdoc/>
		protected override void TranslationChangedCallback (TranslationString translationString)
		{
			base.TranslationChangedCallback (translationString);
			UseUnderline = true;
		}
	}
}

