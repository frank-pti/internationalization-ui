/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Gtk;
using Internationalization;

namespace InternationalizationUi.Mnemonic
{
    /// <summary>
    /// Translatable mnemonic icon toggle button.
    /// </summary>
	public class TranslatableMnemonicIconToggleButton: TranslatableIconToggleButton
	{
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="InternationalizationUi.Mnemonic.TranslatableMnemonicIconToggleButton"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="icon">Icon.</param>
		public TranslatableMnemonicIconToggleButton(I18n i18n, string resource, string text, Image icon):
			this(new TranslationString(i18n, resource, text), icon, Orientation.Horizontal)
		{
		}

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="InternationalizationUi.Mnemonic.TranslatableMnemonicIconToggleButton"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        /// <param name="icon">Icon.</param>
		public TranslatableMnemonicIconToggleButton(TranslationString translation, Image icon):
			this(translation, icon, Orientation.Horizontal)
		{
		}

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="InternationalizationUi.Mnemonic.TranslatableMnemonicIconToggleButton"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="orientation">Orientation.</param>
		public TranslatableMnemonicIconToggleButton(I18n i18n, string resource, string text, Image icon, Orientation orientation):
			this(new TranslationString(i18n, resource, text), icon, orientation)
		{
		}

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="InternationalizationUi.Mnemonic.TranslatableMnemonicIconToggleButton"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="orientation">Orientation.</param>
		public TranslatableMnemonicIconToggleButton(TranslationString translation, Image icon, Orientation orientation):
			base(new TranslatableMnemonicIconLabel(translation, icon, orientation))
		{
		}

        /// <inheritdoc/>
		protected override void InitializeLabel(TranslatableIconLabel label)
		{
			base.InitializeLabel(label);
			label.UseUnderline = true;
			AddMnemonicLabel(label);
		}
	}
}

