/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Internationalization;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable dialog.
    /// </summary>
    public class TranslatableDialog: Gtk.Dialog
    {
        private TranslationString translatableTitle;

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableDialog"/> class.
        /// </summary>
        /// <param name="parent">Parent.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="title">Title.</param>
        /// <param name="arguments">Arguments.</param>
        public TranslatableDialog(Gtk.Window parent, I18n i18n, string resource, string title, params object[] arguments):
            this(parent, i18n.TrObject (resource, title, arguments))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableDialog"/> class.
        /// </summary>
        /// <param name="parent">Parent.</param>
        /// <param name="title">Title.</param>
        public TranslatableDialog(Gtk.Window parent, TranslationString title):
            this(parent, title, Gtk.DialogFlags.Modal)
        {
            translatableTitle = title;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableDialog"/> class.
        /// </summary>
        /// <param name="parent">Parent.</param>
        /// <param name="title">Title.</param>
        /// <param name="flags">Flags.</param>
        /// <param name="buttonData">Button data.</param>
        public TranslatableDialog (Gtk.Window parent, TranslationString title, Gtk.DialogFlags flags, params object[] buttonData):
            base(title.Tr, parent, flags, buttonData)
        {
            title.Changed += HandleTitleChanged;
            translatableTitle = title;
        }

        private void HandleTitleChanged (TranslationString translationString)
        {
            base.Title = translationString.Tr;
            translatableTitle = translationString;
        }

        /// <summary>
        /// Gets the title.
        /// </summary>
        /// <value>The title.</value>
        public new TranslationString Title {
            get {
                return translatableTitle;
            }
        }
    }
}

