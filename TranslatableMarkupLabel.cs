/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Gtk;
using Internationalization;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable markup label.
    /// </summary>
	public class TranslatableMarkupLabel: Label
	{
		private TranslationString translation;

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableMarkupLabel"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="arguments">Arguments.</param>
		public TranslatableMarkupLabel(I18n i18n, string resource, string text, params object[] arguments):
			this(new TranslationString(i18n, resource, text, arguments))
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableMarkupLabel"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
		public TranslatableMarkupLabel(TranslationString translation): base()
		{
			Translation = translation;
			UseMarkup = true;
		}

        /// <summary>
        /// Gets or sets the translation.
        /// </summary>
        /// <value>The translation.</value>
		public TranslationString Translation
		{
			get {
				return translation;
			}
			set {
				if (translation != null)
				{
					translation.Changed -= HandleTranslationChanged;
				}
				translation = value;
				if (translation != null)
				{
					translation.Changed += HandleTranslationChanged;
					HandleTranslationChanged(translation);
				}
			}
		}

		private void HandleTranslationChanged (TranslationString translationString)
		{
			Application.Invoke(delegate {
				TranslationChangedCallback(translationString);
			});
		}

        /// <summary>
        /// Translations the changed callback.
        /// </summary>
        /// <param name="translationString">Translation string.</param>
		protected virtual void TranslationChangedCallback(TranslationString translationString)
		{
			string markup = "";
			if (translationString != null) {
				string tr = translationString.Tr;
				markup = UseUnderline ? tr : tr.Replace("_", "");
			}

			Markup = markup;

			if (markup.Length == 0) {
				Visible = false;
			}
		}
	}
}

