using Gtk;
using Internationalization;
/*
 * An internationalization library for DotNet - Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace InternationalizationUi
{
    /// <summary>
    /// Translatable label.
    /// </summary>
    public class TranslatableLabel : Label
    {
        private TranslationString translation;

        /// <summary>
        /// Internal constructor to support wrapping of native object handles.
        /// </summary>
        /// <param name="raw">Pointer to the C object.</param>
        /// <remarks>This is an internal constructor, and should not be used by user code.
        /// It is required by the GTK# environment.</remarks>
        public TranslatableLabel(IntPtr raw) :
            base(raw)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableLabel"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="text">Text.</param>
        /// <param name="arguments">Arguments.</param>
        public TranslatableLabel(I18n i18n, string resource, string text, params object[] arguments) :
            this(new TranslationString(i18n, resource, text, arguments))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalizationUi.TranslatableLabel"/> class.
        /// </summary>
        /// <param name="translation">Translation.</param>
        public TranslatableLabel(TranslationString translation)
            : base()
        {
            Translation = translation;
        }

        /// <summary>
        /// Gets or sets the translation.
        /// </summary>
        /// <value>The translation.</value>
        public TranslationString Translation
        {
            get
            {
                return translation;
            }
            set
            {
                if (translation != null) {
                    translation.Changed -= HandleTranslationChanged;
                }
                translation = value;
                if (translation != null) {
                    translation.Changed += HandleTranslationChanged;
                    HandleTranslationChanged(translation);
                }
            }
        }

        private void HandleTranslationChanged(TranslationString translationString)
        {
            Application.Invoke(delegate {
                TranslationChangedCallback(translationString);
            });
        }

        /// <summary>
        /// Translations the changed callback.
        /// </summary>
        /// <param name="translationString">Translation string.</param>
        protected virtual void TranslationChangedCallback(TranslationString translationString)
        {
            if (translationString != null) {
                string tr = translationString.Tr;
                if (UseMarkup) {
                    Markup = BuildText(tr);
                } else {
                    Text = BuildText(tr);
                }
            } else {
                Text = "";
            }

            if (Text.Length == 0) {
                Visible = false;
            }
        }

        private string BuildText(string translation)
        {
            return UseUnderline ? translation : translation.Replace("_", "");
        }
    }
}

